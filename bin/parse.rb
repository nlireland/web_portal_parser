#!/usr/bin/env ruby
# frozen_string_literal: true

require 'fileutils'
require "#{__dir__}/../lib/parsed_text_indexer.rb"

puts 'started'
if $PROGRAM_NAME == __FILE__
  # do things
  ARGV[0]
  InternetArchiveSolr::ParsedTextIndexer.new ARGV[0]
  puts 'parser ok'
  puts 'parser finished'
end
puts 'finished'
