# frozen_string_literal: true

require 'logger'
require 'fileutils'
require 'json'
require 'rsolr'
require 'yaml'
require 'date'
require 'pathname'
require 'uri'
require 'addressable/uri'

APP_CONFIG = YAML.load_file(File.dirname(__FILE__) + '/../config/config.yml')

module InternetArchiveSolr
  # Class to index parsetext data from Internet Archive.
  class ParsedTextIndexer
    def initialize(path_to_parse = nil)
      path_to_parse ||= ''
      @main_path = path_to_parse
      @keys_to_delete = APP_CONFIG['keys_to_delete']
      @files = load_all_files
      @error_count = 0
      @total_lines = 0
      @total_add = 0
      @solr = RSolr.connect url: APP_CONFIG['solr_url']
      setup_logger
      @files.each do |file|
        parse(file)
      end
      puts "Total Resources Processed: #{@total_lines}. Total Resources Added: #{@total_add}."
    end

    private

    def load_all_files
      # this returns an array of all the files which are to be parsed.
      files = []
      Dir["#{@main_path}/*.json"].each do |file|
        filepath = @main_path + Pathname.new(file).basename.to_s
        files << filepath
      end
      files.uniq
    end

    def parse(file)
      f = File.open(file)
      puts f.path
      @line_count = 0
      @commit_count = 0
      @filename = File.basename(f.path)
      f.each_line do |line|
        @line_count += 1
        begin
          resource = JSON.parse(line.gsub(/^.*?(?={")/, ''))
          header = line[0..(line.index('{"') - 1)].strip
        rescue JSON::ParserError => e
          @logger.error(e)
          next
        end
        next unless valid_url?(header.split[0]) && (resource['errorMessage'] || resource['type'])

        resource['url'] = header.split[0]
        resource = map_wd_schema(resource) unless resource['digest'].nil?
        resource = media_map_wb_schema(resource, header) unless resource['errorMessage'].nil?
        begin
        solr_add(resource) if resource['id']
        rescue StandardError => e
          puts resource
      end
      end
      @solr.commit
      puts "Lines #{@line_count}, Commited #{@commit_count}"
      @total_lines += @line_count
      @total_add += @commit_count
    end

    def delete_keys(json)
      @keys_to_delete.each do |key|
        json.delete(key)
      end
      json
    end

    def media_map_wb_schema(json, header)
      json['wayback_date'] = @filename.gsub(/\d{14}/).first
      json['id'] = json['wayback_date'] + header.split[1]
      json['hash'] = header.split[1]
      json['url'] = header.split[0]
      json['category'] = 'social_media' if social_media?(json['url'])
      json['type'] = json['errorMessage'].split[8][12..]
      process_keys(json)
      json
    rescue StandardError => e
      @logger.error(e)
    end

    def process_keys(json)
      json['category'] = 'social_media' if social_media?(json['url'])
      json['links'] = process_outlinks(json['outlinks']) if json['outlinks']
      copy_keys(json)
      type_copy(json)
      delete_keys(json)
    end

    def setup_logger
      log_path = APP_CONFIG['log_path']
      logger_path = File.join(log_path, "parser_log-#{DateTime.now.strftime('%d-%m-%Y_%H-%M-%S')}.log")
      FileUtils.touch(logger_path) unless File.file? logger_path
      @logger = Logger.new logger_path
    end

    def map_wd_schema(json)
      json['id'] = "#{json['date']}#{json['digest']}"
      process_keys(json)
      json
    end

    def copy_keys(json)
      json['resourcename'] = json['url']
      json['url_norm'] = normalize(json['url'])
      json['host'] = URI.parse(json['url']).host
      json['wayback_date'] = json.delete('date') if json['date']
      json['status_code'] = json.delete('code') if json['code']
      json['crawl_year'] = json['wayback_date'][0, 4]
      json['hash'] = json.delete('digest') if json['digest']
      json['content_length'] = json.delete('length') if json['length']
      json['content'] = tokenise(json['url']) unless json['content']
      json
    end

    def tokenise(url_key)
      url_key.downcase.scan(/\w+/).join(' ')
    end

    def solr_add(json)
      @solr.add(json)
      @commit_count += 1
    rescue StandardError => e
      @logger.error(e)
      @error_count += 1
    end

    def type_copy(json)
      json['content_type_norm'] = mime_map(json['type']) unless mime_map(json['type']).nil?
      json
    end

    def mime_map(type_key)
      # puts type_key
      case type_key
      when 'text/html', 'application/xhtml+xml'
        'html'
      when 'application/pdf'
        'pdf'
      when /image./
        'image'
      when /video./
        'video'
      when /audio./
        'audio'
      end
    end

    def social_media?(url_key)
      !URI(url_key).host.match(/twitter\.com$|youtube\.com$|facebook\.com$/).nil?
    end

    def valid_url?(url_key)
      URI(url_key)
    rescue StandardError => e
      @logger.error("Bad URL #{url_key}")
      @logger.error(e)
      false
    end

    def normalize(url_key)
      uri = Addressable::URI.parse(url_key)
      uri.normalize.to_s.downcase
    rescue StandardError => e
      @logger.error("Bad URL #{url_key}")
      @logger.error(e)
      'errored'
    end

    def process_outlinks(outlinks_key)
      links = []
      outlinks_key.each do |link_key|
        links << normalize(link_key['url'])
      end
      links
    end

    def measure(&block)
      start = Time.now
      block.call
      puts "#{block.inspect} - took #{Time.now - start}\n"
    end
  end
end
