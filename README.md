# README #

Simple script to convert Internet Archive "Parsed Text" format file into a Solr index (based on the [webarchive-discovery](https://github.com/ukwa/webarchive-discovery/tree/master/warc-indexer/src/main/solr/solr7/discovery/conf) schema)

### Install ###

Just git pull the repository, and copy config/config.yml.EXAMPLE to non example file and update the values in them according to your machine setup.

#### To install on Linux ####

###### Install Ruby ######

```
yum install ruby
yum install ruby-devel
```

###### Install Git ######
```
yum install git
git config --global user.name "Your Actual Name"
git config --global user.email "Your Actual Email"
```

###### Folder Setup #######
```
git clone https://bitbucket.org/nlireland/web_portal_parser
cd /usr/local/web_portal_parser
```

###### Ruby Gems Bundle #######
```
bundle install
```

- set config/config.yml.EXAMPLE

###### Setup Solr Core #######

Copy the folder `./solrconf/webarchive` to the Solr data directory
and give ownership to the solr user e.g.

```
sudo cp -r webarchive/ /var/solr/data/
sudo chown -r solr:solr /var/solr/data/webarchive
```

### Running the script ###

* To run the script, do `ruby bin/parse.rb <folder> `
